:: This builds the docker container.
docker build . --tag frontend

:: This runs the docker container from port 8081.
docker run --rm -d -p 8081:8081 --name bnm frontend
