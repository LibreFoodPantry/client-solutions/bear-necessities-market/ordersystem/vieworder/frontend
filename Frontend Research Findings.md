**Libre Food Pantry
ViewOrder Frontend Technologies**




**Introduction:**
This document will cover the different technologies that will be used to implement the FrontEnd of the ViewOrder function for LFP.

**HTML:**
HTML (Hypertext Markup Language) is a markup language used to create web documents designed for web browsers. The syntax for HTML is different than other programming languages, looking something like this

```
1 <!DOCTYPE html>
2 <html>
3		<head>
4			<title>Text shown in Browser Tab Bar</title>
5		</head>
6
7		<body>
8			<div class =”italic-sarif-red”>
9				<p> “Text Elements, Buttons, etc” </p>
10			</div>
11		</body>
12 </html>
```
At first this looks intimidating, but it’s actually fairly simple, and like normal programming there are many different correct ways to create something. When something is within `<brackets>` like this, that is called an element. Tags are how different HTML elements are specified. For example, the element on line 9 has a tag of “p”, which specifies that the element is wrapping a paragraph within it. The `</p>` is called the closing tag. Some elements don’t require closing tags, such as the `<img />` tag, which is the tag for an image. Really all you need to do is learn how the different elements work with each other and then you will be able to make a basic page with HTML, though this page will look pretty barren without CSS.

**CSS:**
CSS (Cascading Style Sheets) is what is called a “style sheet language”. CSS is almost always used with an HTML document. If you notice on line 8 of the sample HTML code the `<div>` element has the “class” attribute. Some elements are able to have specific attributes, and this “class” attribute is how the HTML file interacts with the CSS file. The CSS file can look something like this

```
1 .italic-sarif-red{
2 	font-family: serif;
3	font-style: italic;
4	color: red;
5 }
```
“italic-sarif-red” is what is called the “selector” in a CSS file. When this CSS file is imported to the HTML document, it will see every element with the class attribute of “italic-sarif-red” and change the format of whatever is inside of that element to conform to the properties we have defined in the CSS file. There are a ton of properties you can add to CSS selectors, but for text this will be good enough.

**Javascript:**
Combining this HTML and this CSS, we now have a paragraph element displayed on our page with serif italic font colored red. But how do we add cool things like scripts and interactivity? That is where Javascript comes in.

Javascript is a fairly simple high-level programming language that adds functionality to the application, such scripts and HTML object manipulation. Javascript is inserted into HTML using the element `<script> Javascript Here </script>`. With this, you now have the ability to dynamically edit the web page through changing the HTML elements directly with Javascript code. This is a little hard to show without visuals, but here is a helpful video that shows how exactly it works.4: How to Include JavaScript in Our HTML | JavaScript Tutorial | Learn JavaScript | For Beginners

**React.js**

Using HTML, CSS, and Javascript together gets the job done but can be slow due to how many things you need to keep track of. This is where the React Javascript library comes into play. By adding this library to your project, the entire structure of your HTML files will change. In fact, you don’t even need to use HTML files. These can be replaced with .jsx files, which is a syntax extension of Javascript that allows for HTML within the javascript. 

So rather than having an HTML file that can have Javascript elements, we can have a Javascript file that can have HTML elements which allows for some cool tricks, such as passing data between pages. The structure of the project also changes, as page elements are now divided into React Components to use freely across any page. All of our HTML code will be placed within a Render function at the very bottom of our Javascript code. Here is an example of a JSX file that exports a React component of a home page with an incrementing button on it.
```
import React from "react";
//components
class HomePage extends React.Component {
       constructor(props) {
    	      super();
    	      this.state = {
              number: 0
    	      };
        }

	  makeNumberBigger(e) {
   		 var newNum = this.state.number;
   		 newNum++;
   		 this.setState({ number: newNum });
    });
  }


  render() {
    return (
      <div id="home" className="container">
        <main role="main" className="py-5 text-center bd-content">
          <h3 className="mx-3">
             Welcome to my web page!
          </h3>
          <h1 className="display-1">{this.state.number}</h1>
          <button type="button" onClick={this.makeNumberBigger}>
            Push me to make the number go higher!
          </button>
        </main>
      </div>
    );
  }
}

export default HomePage;
```
When the “State” of the react component is changed, the reference to the state in the HTML “reacts” to the change in state and updates the display, hence the name of the React library.

It should also be noted that the “class” attribute in HTML is now changed to “className” when being used in Javascript

At the top in the Constructor you can notice a list of elements called States. This State list stores dynamic info. A React component should use this state list to store information that the component itself can change. However if there is information that is stored that is changed by a different component, then this information is stored in the Props list. This organizes the data into passed-in data and data that is local to that component. 

**Setting Up a React Project**

First thing we need to do is make sure we have Node.JS installed. This allows for easy downloading of open source React libraries and components. Node.JS can be found here: https://nodejs.org/en/download/

There already exists good official documentation for setting up a React file: https://reactjs.org/docs/create-a-new-react-app.html. 
When this tutorial references “toolchains”, it just means to run those commands in your console at the desired directory of your project. For example “npx create-react-app app-name” would be what you would want to type in for creating a react application.

After we have our project set up we now have a folder containing our project template which is now just a whole mess of folders. We can run “npm start” in this folder to start our application. Here is a good video that briefly goes over the folder structure of your React project ReactJS Tutorial - 3 - Folder Structure.

This Frontend folder for ViewOrder has been created here: https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/ordersystem/vieworder/frontend



