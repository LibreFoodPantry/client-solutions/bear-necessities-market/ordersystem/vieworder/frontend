FROM node:8.7.0-alpine

# This is where you start (when you first go to command line) in the container. 
WORKDIR /app

COPY package.json /app
COPY package-lock.json /app

#This runs the command npm install.
RUN npm install

COPY . /app

#This runs the command node index.js so that the page can be viewed.
CMD node index.js



EXPOSE 8081


