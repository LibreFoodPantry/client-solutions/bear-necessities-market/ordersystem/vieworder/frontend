## Purpose

Frontend for BNM View Order module, built off of the ApproveOrder module.


## Install Instructions

NOTE: We have encountered an "events.js:174  throw er; // Unhandled 'error' event" and "Error: Listen EADDRINUSE: address alread in use :::8081" when running the start.bat file on both Windows and Linux!  Rerunning this file may fix it.  Further development is needed for this bug.

### Windows 10 Install Instructions

1. Check Virtualization settings:
Search 'turn windows features on or off'
Check box Hyper-V

2. Check Windows 10 Pro:
Search 'settings'
Click 'update and security'
Click 'activation'
Check Windows -> Edition -> 'Windows 10 Pro'
(upgrade to enable Hype-V settings in bios)

3. Install Docker Desktop:
https://www.docker.com/products/docker-desktop
Follow the install instructions
Check if installed using 'docker --version'
Check Docker is running: toolbar -> '^' icon -> white whale

4. Install Git:
https://git-scm.com/downloads
Follow the install instructions
Check if installed using 'git --version'

5. Install Node.js
https://nodejs.org/en/download/
Follow the install instructions
Check if Node installed using 'node -v'
Check if NPM installed using 'npm -v'

6. Open GitBash:
Run the following commands:
cd 'desired directory'
git clone 'BNM -> ViewOrder -> FrontEnd -> Clone with HTTPS'
cd frontend
./start.bat


7. Run index.js
index.js should be in the top-most directory labeled ~/'desired directory'/frontend
Run the following command:
node index.js

8. Ensure the Docker container is running: Open Docker Desktop -> Check bnm frontend is running

9. Stop the container: ./stop.bat

### Debian/Ubuntu/Linux Mint

1. Create GitLab account
https://gitlab.com/

2. Update your repository: 
Run the following command:
sudo apt-get update

3. Install Git:
Check if installed using 'git --version'
Run the following command:
sudo apt-get install git

4. Clone FrontEnd:
Navigate to BNM -> ViewOrder -> FrontEnd
Click 'clone'
Copy link 'Clone with HTTPS'
Run the following command:
git clone 'HTTPS link'

5. Install Docker:
Run the following commands then restart your machine:
sudo apt-get install docker.io
usermod -aG docker "${USER}"

6. Run Docker:
Run the following commands:
docker build . --tag frontend
docker run -rm -d -p 8081:8081 --name bnm frontend

7. Install Node.JS:
Run the following command:
sudo apt-get install node.js

8. Run index.js:
Run the following commands
cd 'directory containing index.js'
node index.js

## Current Status

Start and stop the container using: start.bat and stop.bat.
A Pipeline is set up to measure the code quality of merge requests.
The app has a submit button, footers for the university website, an order popup, and some images.