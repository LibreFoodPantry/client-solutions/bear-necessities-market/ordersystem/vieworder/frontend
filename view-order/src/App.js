import React, { Component } from 'react';
import Header from './Components/Header';
import OrderGrid from './Components/OrderGrid';
import Footer from './Components/Footer';
import AltFooter from './Components/AltFooter';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Components/styling/wne.css'




export default class App extends Component {
  render() {
    return (
      <form>
        <Header />
        <OrderGrid />
        <AltFooter />
      </form>
    );
  }
}
