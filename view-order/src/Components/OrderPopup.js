import React from 'react';
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from 'react-bootstrap/Button';

class OrderPopup extends React.Component{
  constructor(props) {
      super();
      this.state = {};
  }

  CloseOrder(){
    this.props.DisplayOrder(false);
  };

render(){
  return(
  <Card style={{position: "absolute", right: "0%", top: "0px", height: "100%", width: "30%", zIndex: 9}} >
    <CardContent>
      <Typography variant="h4">
          {this.props.orderID}
      </Typography>
      <Typography variant="body1" color="textPrimary" style={{fontSize:25}}>
          Dietary Restrictions:  
      </Typography>
      <Typography variant="body1">
              {this.props.dietaryrestrictions}
          </Typography>
          &#160; &#160;
      <Typography variant="body1" color="textPrimary" style={{fontSize:25}}>
          Items Requested: 
      </Typography>
      <Typography variant="body1">
              {this.props.items}
          </Typography>
          &#160; &#160;
      <CardActions>
          <Button variant="primary" onClick={this.props.CloseOrder}>
              Close Preview 
          </Button>
          </CardActions>
      </CardContent>
    <CardContent>
    </CardContent>
    </Card>
    );
}};

export default OrderPopup;