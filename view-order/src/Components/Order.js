import React from 'react';
import Button from 'react-bootstrap/Button';
import { createMuiTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import ToggleButtons from './ToggleButtons';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import 'reactjs-popup/dist/index.css';
import OrderPopup from './OrderPopup';


//Plan: Restructure this into a react.component subclass as an Order.js to allow for states and props
class Order extends React.Component{
    constructor(props) {
        super();
        this.state = {};
        this.DisplayOrder = this.DisplayOrder.bind(this);
        this.CloseOrder = this.CloseOrder.bind(this);    

    }

    //function to display order information card
    DisplayOrder(){
        this.setState({showOrder: true})
    };

    CloseOrder(){
        console.log("Order Closed")
        this.setState({showOrder: false})
    };

render(){   
    
    const mystyle = createMuiTheme ({
        root: {
            minWidth: 275,
            height: "100%",
            width: 500,
            marginLeft: 200,
            direction: "row",
            justifyContent: 'center',
            alignItems: 'center',
        },

    });
    let orderPopup = <div></div>
    if(this.state.showOrder){
        orderPopup = <OrderPopup orderID={this.props.orderID} dietaryrestrictions={'cheeze'} items={'bread, water'} CloseOrder={this.CloseOrder}/>
                                       //this.state.order              
    }
        return (
            <div style={{position: "relative"}}>
                <Card style={mystyle.root} spacing={30}>
                    <CardContent>
                        <Typography style={{fontSize:14}} color="textSecondary" gutterBottom>
                            Placed {this.props.OrderDate}
                        </Typography>
                        <Typography variant="h5" component="h2">
                            {this.props.orderID}
                        </Typography>
                        <Typography style={mystyle.pos} color="textSecondary">
                            Connected Email: {this.props.Email}
                        </Typography>
                        <Typography style={mystyle.pos} color="textSecondary">
                            Number of Items: {this.props.ItemQuantity}             
                        </Typography>
                        <Button style={{marginTop: "5%"}} variant="primary" onClick={this.DisplayOrder} >
                                View Order
                            </Button>
                    </CardContent>
                    <CardActions>
                        <ToggleButtons style={mystyle.toggleContainer}></ToggleButtons>
                    </CardActions>
                </Card>         
                {orderPopup} 
            </div>
        );
    }
}
export default Order;