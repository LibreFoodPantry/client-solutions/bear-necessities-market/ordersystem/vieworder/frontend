import React from 'react';
import SubmitButton from './SubmitButton';
import footerlogo from "./images/wne-mag-footer-logo.png"


class AltFooter extends React.Component{
    constructor(props){
    super();
    this.state = {};
    //bindings

    //
    }


   
render() {
    const footerstyles = {
        center: {
        	textAlign: "center",
        	backgroundColor: "#f2f2f2",
	        padding: 30,
	        borderTop: 5,
            borderStyle: "solid",
            borderColor: "#ccc",
        },
        centerimg: {
            width: "100%",
            maxWidth: 450,
        },
        a: {
            color: "#c98509",
            }
         };
        
        return (
            <div style={footerstyles.center}>
                <div class="logo" data-partial="logo"><a href="//www.wne.edu" title="Western New England University Website Home Page"><img alt="wne-mag-footer-logo.png" src={footerlogo} width="450"/></a></div>
                <br/>
                <div class="footer-links" data-partial="footer-links">
                    <a href="//www1.wne.edu/news/index.cfm">News</a>
                    &#160; &#160; <a href="//www1.wne.edu/become-a-student/request-info/index.cfm">Contact Us</a>
                    &#160; &#160; <a href="//www1.wne.edu/directory/index.cfm">Directory</a>
                    &#160; &#160; <a href="//www1.wne.edu/human-resources/careers.cfm">Employment</a>
                        </div>
                <br/>
                <div class="footer-links" data-partial="footer-links"><span class="street-address"></span></div>
                <div class="footer-links" data-partial="footer-links"><span class="street-address">1215 Wilbraham Road,</span> <span class="locality">Springfield,</span> <span class="region">MA</span> <span class="postal-code">01119</span></div>
                <div class="footer-bottom">
                    <div class="footer-contact" data-partial="footer-contact">
                        <div class="contact"><span class="tel"><br/> </span></div>
                    </div>
                        <div class="footer-copyright" data-partial="footer-copyright">
                        <div><small><span class="copyright">Copyright &#169;
                            <script></script> <a>2021 | </a>
                            <a href="privacy/index.cfm">Privacy Policy</a> |&#160;</span><a href="site-map/index.cfm">Site Map</a></small>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default AltFooter;