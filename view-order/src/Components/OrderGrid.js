import React from 'react';
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Container from 'react-bootstrap/Container'
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Order from './Order.js';
import OrderPopup from './OrderPopup';
import SubmitButton from './SubmitButton';

class OrderGrid extends React.Component{
    constructor(props){
    super();
    this.state = {};
    //bindings
    this.DisplayOrder = this.DisplayOrder.bind(this);  
    this.CloseOrder = this.CloseOrder.bind(this);
    //
    }

    
    //function to display order information card
    DisplayOrder(orderID){
        console.log("Order Displayed")
        //api call for orderID
        this.setState({currentOrder: {
            orderID: orderID, //update with API response

        }})
    };

    CloseOrder(){
        console.log("Order Closed")
        this.setState({currentOrder: false})
    };


render(){
    const useStyles = makeStyles((theme) => ({
        container: {
            display: 'grid',
            direction: "row",
            justifyContent: 'center',
            alignItems: 'center',
            alignContent: 'center',
            gridTemplateColumns: 500,
            gridGap: theme.spacing(3),
        },
        expand: {
            transform: 'rotate(0deg)',
            marginLeft: 'auto',
            transition: theme.transitions.create('transform', {
              duration: theme.transitions.duration.shortest,
            }),
          },
          expandOpen: {
            transform: 'rotate(180deg)',
          },
    }));
    let orderPopup = <div></div>
    if(this.state.currentOrder){
        orderPopup = <OrderPopup orderID={this.state.currentOrder.orderID} dietaryrestrictions={'cheeze'} items={'bread, water'} CloseOrder={this.CloseOrder}/>
                                       //this.state.order              
    }
    return (
        <div>
            <Container>
                <Row >
                    <Col style={{marginTop: 30, marginBottom:30}}>
                        <Grid container spacing={5} justify='center'>
                            <Grid item xs={12}>
                                <Order DisplayOrder={this.DisplayOrder} orderID={"OR2366236"} OrderDate={"2/4/2017"} Email="gogogadk@wne.edu" ItemQuantity={"7"} />
                            </Grid>
                            <Grid item xs={12}>
                                <Order DisplayOrder={this.DisplayOrder} orderID={"OR653"} OrderDate={"3/7/2395"} Email="harh@wne.edu" ItemQuantity={"23"} />
                            </Grid>
                        </Grid>
                    </Col>
                </Row>
                <div >
                    <SubmitButton />
                </div>
            </Container>
        </div>
    );
    }
    //line 91 {orderPopup}
}
export default OrderGrid