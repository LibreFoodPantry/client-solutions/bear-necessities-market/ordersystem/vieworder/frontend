import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import SubmitButton from './SubmitButton';


class Footer extends React.Component{
    constructor(props){
    super();
    this.state = {};
    //bindings

    //
    }


   
render() {
    const footerstyles = {
        appBar: {
            top: 'auto',
            bottom: 0,
        },
        grow: {
            flexGrow: 100,
        },
         };
        
        return (
            <div>
                <AppBar edge="start" position="fixed" color="primary" position="static" style={footerstyles.appBar}>
                    <SubmitButton type='submit'></SubmitButton>
                </AppBar>
            </div>
        );
    }
}
export default Footer;