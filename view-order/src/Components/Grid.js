import clsx from 'clsx';    
import React from 'react';
import Collapse from '@material-ui/core/Collapse';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import IconButton from '@material-ui/core/IconButton';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Container from 'react-bootstrap/Container'
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import ToggleButtons from './ToggleButtons';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Order from './Order.js'

    //function to display order information card
    function DisplayOrder(){
        console.log("Order Displayed")
    };

    function CloseOrder(){
        console.log("Order Closed")
    };

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'grid',
        direction: "row",
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        gridTemplateColumns: 500,
        gridGap: theme.spacing(3),
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
          duration: theme.transitions.duration.shortest,
        }),
      },
      expandOpen: {
        transform: 'rotate(180deg)',
      },
}));


export default function GridGen() {
    const classes = useStyles();
    return (
        <div>
            <Container>
                <Row >
                    <Col style={{marginTop: 30, marginBottom:30}}>
                        <Grid container spacing={5} justify='center'>
                            <Grid item xs={12}>
                                <Order></Order>
                            </Grid>
                            <Grid item xs={12}>
                                <Order></Order>
                            </Grid>
                        </Grid>
                    </Col>
                    <Col style={{marginTop: 30, marginBottom:30}}>
                         <Card >
                            <CardContent>
                                <Typography variant="h4">
                                    OR12838
                                </Typography>
                                <Typography variant="body1" color="textPrimary" style={{fontSize:25}}>
                                    Dietary Restrictions:  
                                </Typography>
                                <Typography variant="body1">
                                        list of dietary restrictions goes here
                                    </Typography>
                                <Typography variant="body1" color="textPrimary" style={{fontSize:25}}>
                                    Items Requested: 
                                </Typography>
                                <Typography variant="body1">
                                        list of items goes here
                                    </Typography>
                                <CardActions>
                                    <Button variant="primary" onClick={CloseOrder}>
                                        Close Preview 
                                    </Button>
                                    </CardActions>
                                    </CardContent>
                                    <CardContent>
                                </CardContent>
                    </Card>               
                </Col>
              </Row>
            </Container>
        </div>
    );
    }