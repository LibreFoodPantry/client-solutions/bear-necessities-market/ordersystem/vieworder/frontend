import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import Icon from '@material-ui/core/Icon';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import { green } from '@material-ui/core/colors';
import { red } from '@material-ui/core/colors';
import { yellow } from '@material-ui/core/colors';

const useStyles = makeStyles((theme) => ({
  toggleContainer: {
    margin: theme.spacing(2, 0),
    
  }
  ,

}));
const GreenCheckIcon = {
    color: green[500],
    '&$checked': {
      color: green[1200],
    }
};
const RedCloseIcon = {
    color: red[500],
    '&$checked': {
      color: red[1200],
    }
};

const YellowRadioButtonUncheckedIcon = {
    color: yellow[500],
    '&$checked': {
      color: yellow[1200],
  }
};


export default function Setup() {
  const [status, setStatus] = React.useState("Pending");

  const handleStatus = (event, newStatus) => {
    if (newStatus != null) {
      setStatus(newStatus);
    }
  };

  const classes = useStyles();

  return (
    <div className={classes.toggleContainer}>
      <ToggleButtonGroup
        value={status}
        exclusive
        onChange={handleStatus}
        aria-label="status"
      >
        <ToggleButton value="Pending" aria-label="Pending" style={YellowRadioButtonUncheckedIcon}>
          <RadioButtonUncheckedIcon />
        </ToggleButton>
        <ToggleButton value="Approved" aria-label="Approved"style={GreenCheckIcon}>
          <CheckIcon />
        </ToggleButton>
        <ToggleButton value="Rejected" aria-label="Rejected"style={RedCloseIcon}>
          <CloseIcon />
        </ToggleButton>
        <div>
          Status: {JSON.stringify(status, null, 2)}
        </div>
      </ToggleButtonGroup>
    </div>
  );
}