import React from 'react';
import AppBar from '@material-ui/core/AppBar'
import logo from '../lfp-logo.jpg'



export default class Header extends React.Component {

    constructor(props){
        super();
        this.state = {};
    }
    //bindings
    render() {
        const wneBlue = "#003366"

        return (
            <div>
                <AppBar style={{backgroundColor: wneBlue}} position="static" >
                    <div>
                        <div style={{display:"inline-flex", verticalAlign:"middle", paddingRight:"20px" }}>
                            <img alt="logo" src={logo} height={80} width={80}/>
                        </div>
                        <div style={{display:"inline-flex", verticalAlign:"middle"}}>
                            <h1 style ={{color: "#ffffff"}} class="font-weight-bold" >Bear Necessities Market</h1>
                        </div>
                    </div>
                </AppBar>
            </div>
        );
    }
}